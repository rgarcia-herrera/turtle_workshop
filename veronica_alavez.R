#### Ejercicios de programación usando el paquete "TurtleGraphics"
## Verónica Alavez Salgado

# library("grid")  # este no lo usas! mejor no cargarlo :-)
library("TurtleGraphics")

### Crear una función para formar una espiral que abre
turtle_init()

espiral <- function(size=500, ancho=2){
  turtle_do({
    for(j in 1:size){
      turtle_forward((j)/(size))
      turtle_left(ancho)
    }
  })
}

espiral()
espiral(200,3)


## Crear una función para generar un patron de moire

turtle_init()

moire_poligono<- function(repeticiones=8,lados=5,largo=10){
  turtle_do({
    for (i in 1:repeticiones) {
      turtle_up()
      turtle_goto((i+5),(i+5))
      turtle_down()
      for (l in 1:20) {  # epa, ¿qué es este 20? esto se llama "número mágico", es más legible crear una variable con un nombre explicativo y darle valor 20
        # mejor crear la funcion poligono(x, y, lados, largo) ¿no? y hacer más claro todo lo que sigue
        turtle_up()
        here<-turtle_status()
        turtle_goto(((here$TurtleStatus$x)+3),((here$TurtleStatus$y)+3))
        turtle_down()
        for (x in 1:lados) {
          turtle_forward(largo)
          turtle_right(360/lados)
        }
      }
    }
  })
}
moire_poligono()
moire_poligono(10,9,5)
# bonitos patrones!